package com.auca.userManagement.impl;

import java.util.List;

import com.auca.userManagement.dao.NotificationDao;
import com.auca.userManagement.domain.Notification;
import com.auca.userManagement.service.NotificationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class NotificationServiceImp implements NotificationService {

    @Autowired
    private NotificationDao dao;

    @Override
    public List<Notification> findByUser(String uuid) {
        return dao.userNotification(uuid);
    }

    @Override
    public String changeStatus(Notification n) {
        return dao.update(n);
    }

    @Override
    public String create(Notification n) {
        return dao.create(n);
    }

    @Override
    public Notification findByUuid(String uuid) {
        return dao.findByUuid(uuid);
    }
    
}