package com.auca.userManagement.impl;

import com.auca.userManagement.service.FileHandler;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class IFileHandler implements FileHandler {


    /**
     * Function to Save user Profile Picture
     * @param file
     * @param oldFile
     * @param names
     * @return
     */
    @Override
    public String saveProfile(MultipartFile file,String oldFile,String names) {
        String filepath="";
        try{
            File user=new File("user");
            deleteOldProfile(oldFile);
            if(!user.exists()){
                user.mkdir();
                    byte[] bytes = file.getBytes();
                    Path path = Paths.get( user.getPath()+ "/" +names+file.getOriginalFilename());
                    Files.write(path, bytes);
                    filepath=path.toString();
            }else{
                byte[] bytes = file.getBytes();
                Path path = Paths.get( user.getPath()+ "/" +names+file.getOriginalFilename());
                Files.write(path, bytes);
                filepath=path.toString();
            }

        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        return filepath;
    }

    public void deleteOldProfile(String oldFile) {
        File profile = new File(oldFile);
        try{
            if (profile.exists()) {
                FileUtils.cleanDirectory(profile);
                profile.delete();
            }
        }catch(Exception ex){
            ex.getMessage();
        }

    }
}
