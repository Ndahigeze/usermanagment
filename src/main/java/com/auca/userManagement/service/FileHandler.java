package com.auca.userManagement.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileHandler {

    String saveProfile(MultipartFile file,String oldFile,String names);
}
