package com.auca.userManagement.service;

import java.util.List;

import com.auca.userManagement.domain.Notification;

public interface NotificationService {
    
    List<Notification> findByUser(String uuid);
    String changeStatus(Notification n);
    String create(Notification n);
    Notification findByUuid(String uuid);
    
}