package com.auca.userManagement.dao;

import java.util.ArrayList;
import java.util.List;

import com.auca.userManagement.Utility.Msg;
import com.auca.userManagement.domain.Notification;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NotificationDao {
    @Autowired
    private SessionFactory sessionFactory;
    

    /**
     * Creating Nofication
     * @param n
     * @return
     */
    public String create(Notification n ){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {

            session.save(n);
            tx.commit();
            session.flush();
            return Msg.save;
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
            return Msg.error;
        } finally {
            if (session != null && session.isOpen()) {
                session.clear();
                session.close();
            }
        }
    }

    /**
     * Updating Notification
     * @param n
     * @return
     */
    public String update(Notification n) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {

            session.update(n);
            tx.commit();
            session.flush();
            return Msg.update;
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
            return Msg.error;
        } finally {
            if (session != null && session.isOpen()) {
                session.clear();
                session.close();
            }
        }
    }
     
    
     /**
      * Find by Uuid;
      * @param uuid
      * @return
      */
    @SuppressWarnings("unchecked")
    public Notification findByUuid(String uuid) {
        Session session = sessionFactory.openSession();
        Notification n = null;
        try {

            // user = session.get(SystemUser.class, uuid);
            n= (Notification) session.createQuery("from Notification s where s.uuid=:uuid")
                    .setParameter("uuid", uuid).uniqueResult();
        } catch (Exception e) {

        } finally {
            if (session != null && session.isOpen()) {
                session.flush();
                session.clear();
                session.close();
            }
        }

        return n;
    }

    /**
     * retrieving all User Notification
     * @param uuid
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Notification> userNotification(String uuid) {
        Session session = sessionFactory.openSession();
        List<Notification> list = new ArrayList<>();
        try {

            list = session.createQuery("from Notification where userUuid= :v and readStatus= :s")
            .setParameter("v", uuid).setParameter("s",false)
            .list();
        } catch (Exception e) {

        } finally {
            if (session != null && session.isOpen()) {
                session.flush();
                session.clear();
                session.close();
            }
        }
        return list;
    }


}