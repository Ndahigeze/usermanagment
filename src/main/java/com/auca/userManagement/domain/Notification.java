package com.auca.userManagement.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)  
public class Notification implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(updatable=false)
	private String uuid=UUID.randomUUID().toString();
    
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date doneAt;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(nullable = false, updatable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @LastModifiedDate
    private Date lastUpDatedAt;

    private String notification;

    private boolean readStatus=false;

    private String userUuid;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return long return the id
     */
    public long getId() {
        return id;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return String return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return Date return the doneAt
     */
    public Date getDoneAt() {
        return doneAt;
    }

    /**
     * @param doneAt the doneAt to set
     */
    public void setDoneAt(Date doneAt) {
        this.doneAt = doneAt;
    }

    /**
     * @return Date return the lastUpDatedAt
     */
    public Date getLastUpDatedAt() {
        return lastUpDatedAt;
    }

    /**
     * @param lastUpDatedAt the lastUpDatedAt to set
     */
    public void setLastUpDatedAt(Date lastUpDatedAt) {
        this.lastUpDatedAt = lastUpDatedAt;
    }

    /**
     * @return String return the notification
     */
    public String getNotification() {
        return notification;
    }

    /**
     * @param notification the notification to set
     */
    public void setNotification(String notification) {
        this.notification = notification;
    }

    public boolean isReadStatus() {
        return readStatus;
    }

    public void setReadStatus(boolean readStatus) {
        this.readStatus = readStatus;
    }
}