package com.auca.userManagement.controller;

import javax.servlet.http.HttpServletRequest;

import com.auca.userManagement.Utility.EmailHandling;
import com.auca.userManagement.Utility.ResponseBean;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mails")
public class EmailController {
    

     @RequestMapping(value = "/send", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> update(@RequestBody InnerEmail e,
            HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
          
           EmailHandling.genericEmail(e.subject, e.email, e.content); 
           System.out.println(e.subject+e.email+e.content);               
        } catch (Exception ex) {
           ex.printStackTrace();
        }

        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    public static class InnerEmail{
        private String email;
        private String content;
        private String subject;
        
        /**
         * @return String return the email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email the email to set
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return String return the content
         */
        public String getContent() {
            return content;
        }

        /**
         * @param content the content to set
         */
        public void setContent(String content) {
            this.content = content;
        }

        /**
         * @return String return the subject
         */
        public String getSubject() {
            return subject;
        }

        /**
         * @param subject the subject to set
         */
        public void setSubject(String subject) {
            this.subject = subject;
        }

    }


    
}