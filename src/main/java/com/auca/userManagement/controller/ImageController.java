package com.auca.userManagement.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.auca.userManagement.domain.SystemUser;
import com.auca.userManagement.service.ISystemUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pictures")
public class ImageController {
    @Autowired
  private ISystemUserService userService;



    @GetMapping(value = "/{uuid}")
    public ResponseEntity<InputStreamResource> getImage(@PathVariable("uuid") String uuid) throws IOException {
         
         String workingDir = System.getProperty("user.dir");
         System.out.println(uuid+"/");
            SystemUser user=userService.findByUUId(uuid);

        String filePath = workingDir + "/" +user.getProfilePicture().replaceAll("\\\\", "/");

        File file = new File(filePath);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Content-Disposition", "inline; filename=" + file.getName());
        Path path = Paths.get(filePath);
        byte[] b = Files.readAllBytes((path));
        ByteArrayInputStream bis = new ByteArrayInputStream(b);
          System.out.println("Hello CHeck");
        // Files.rea
        // InputStreamResource resource = new InputStreamResource(new
        // FileInputStream(file));
        return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.IMAGE_PNG)
                .body(new InputStreamResource(bis));
    }
}