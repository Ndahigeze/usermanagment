package com.auca.userManagement.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import com.auca.userManagement.Utility.Msg;
import com.auca.userManagement.Utility.ResponseBean;
import com.auca.userManagement.Utility.SMS;

import com.auca.userManagement.domain.Notification;
import com.auca.userManagement.service.NotificationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/notifications")
public class NotificationController {
    
    @Autowired
    private NotificationService notiService;


    /**
     * Save Notification
     * @param request
     * @param n
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> create(HttpServletRequest request,@RequestBody InnerNotification n) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String userToken = request.getHeader("ranga_token");

            if (userToken != null) {
                if (userToken.equalsIgnoreCase(Msg.token)) {

                    Notification noti=new Notification();

                        noti.setReadStatus(false);
                        noti.setNotification(n.notification);
                        noti.setTitle(n.title);
                        noti.setUserUuid(n.user);
                    responseBean.setDescription(notiService.create(noti));
                    responseBean.setCode(Msg.SUCCESS_CODE);
                    responseBean.setObject(n);
                  
                } else {
                    responseBean.setCode(Msg.INCORRECT_TOKEN);
                    responseBean.setDescription("INCCORECT TOKEN ");
                    responseBean.setObject(null);
                }

            } else {

                responseBean.setCode(Msg.TOKEN_NOT_FOUND);
                responseBean.setDescription(" TOKEN NOT FOUND ");
                responseBean.setObject(null);

            }
        } catch (Exception e) {
            responseBean.setCode(Msg.ERROR_CODE);
            responseBean.setDescription("SOMETHING WENT WRONG TRY AGAIN ");
            responseBean.setObject(null);
            e.printStackTrace();
        }

        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }



    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public ResponseEntity<Object> markAsRead(HttpServletRequest request, @PathVariable("uuid") String uuid) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String userToken = request.getHeader("ranga_token");

            if (userToken != null) {
                if (userToken.equalsIgnoreCase(Msg.token)) {
                    System.out.println(uuid);
                   Optional<Notification> n=Optional.ofNullable(notiService.findByUuid(uuid));
                   if(n.isPresent()){
                       System.out.println(n.get().getUserUuid());
                       n.get().setReadStatus(true);
                          responseBean.setCode(Msg.SUCCESS_CODE);
                          responseBean.setDescription(notiService.changeStatus(n.get()));
                          responseBean.setObject(n.get());
                   }else{
                         responseBean.setCode(Msg.ERROR_CODE);
                        responseBean.setDescription("NOTIFICATION NOT FOUND");
                        responseBean.setObject(null);
                   }
                  

                } else {
                    responseBean.setCode(Msg.INCORRECT_TOKEN);
                    responseBean.setDescription("INCCORECT TOKEN ");
                    responseBean.setObject(null);
                }
            } else {
                responseBean.setCode(Msg.TOKEN_NOT_FOUND);
                responseBean.setDescription(" TOKEN NOT FOUND ");
                responseBean.setObject(null);
            }
        } catch (Exception e) {
            responseBean.setCode(Msg.ERROR_CODE);
            responseBean.setDescription("SOMETHING WENT WRONG TRY AGAIN ");
            responseBean.setObject(null);
            e.printStackTrace();
        }

        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * finding all user notifications
     * @param request
     * @param uuid
     * @return
     */
    @RequestMapping(value = "/user/{uuid}", method = RequestMethod.GET)
    public ResponseEntity<Object> findAll(HttpServletRequest request, @PathVariable("uuid") String uuid) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String userToken = request.getHeader("ranga_token");

            if (userToken != null) {
                if (userToken.equalsIgnoreCase(Msg.token)) {
                    responseBean.setCode(Msg.INCORRECT_TOKEN);
                    responseBean.setDescription("NOTIFICATIONS FOUND ");
                    responseBean.setObject(notiService.findByUser(uuid));

                } else {
                    responseBean.setCode(Msg.INCORRECT_TOKEN);
                    responseBean.setDescription("INCCORECT TOKEN ");
                    responseBean.setObject(null);
                }
            } else {
                responseBean.setCode(Msg.TOKEN_NOT_FOUND);
                responseBean.setDescription(" TOKEN NOT FOUND ");
                responseBean.setObject(null);
            }
        } catch (Exception e) {
            responseBean.setCode(Msg.ERROR_CODE);
            responseBean.setDescription("SOMETHING WENT WRONG TRY AGAIN ");
            responseBean.setObject(null);
        }

        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/sms", method = RequestMethod.GET)
    public ResponseEntity<Object> findSendSMS(HttpServletRequest request)throws Exception{
        ResponseBean responseBean = new ResponseBean();


//        SMSHandling.sentSms("Hello", "250785256254");
//        responseBean.setCode(Msg.SUCCESS_CODE);
//        responseBean.setDescription("MESSAGE SEND ");
//        responseBean.setObject(SMSHandling.sentSms("Hello", "250785256254"));
        return new ResponseEntity<Object>(SMS.sendSingleSMS("+250783046679", "Test Sms "), HttpStatus.OK);
    }


    public static class InnerNotification{
        private String notification;
        private String title;
        private String user;

        public String getNotification() {
            return notification;
        }

        public void setNotification(String notification) {
            this.notification = notification;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }
    }
}